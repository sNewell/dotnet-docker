module FSharpApiExample

open Suave
open Suave.Operators

let toJsonStr =
  Json.toJson >> System.Text.Encoding.UTF8.GetString

let zeldaGamesJsonStr =
  [|
    "Link to the Past"; "Breath of the Wild";
    "Ocarina of Time"; "Majora's Mask";
    "Wind Waker"; "Twilight Princess";
  |]
  |> toJsonStr

let zeldaJsonWebPart =
  Filters.GET
  >=> Filters.path "/zelda/games"
  >=> (Successful.OK zeldaGamesJsonStr)
  >=> Writers.setMimeType "application/json; charset=utf-8"

[<EntryPoint>]
let main _ = 
  startWebServer { defaultConfig with bindings = [ HttpBinding.createSimple HTTP "0.0.0.0" 8080]; } zeldaJsonWebPart
  0
