﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace api_example.Controllers
{
  [Route("zelda")]
  [ApiController]
  public class ZeldaController : ControllerBase
  {
    private static string[] GAME_LIST = new string[] {
      "Link to the Past",
      "Breath of the Wild",
      "Ocarina of Time",
      "Majora's Mask",
      "Wind Waker",
      "Twilight Princess"
    };

    // GET zelda/games
    [HttpGet("games")]
    public ActionResult<IEnumerable<string>> Get() =>
      GAME_LIST;
  }
}
