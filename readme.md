# .NET Core & Docker

This is a quick, brief presentation I threw together to talk about .NET Core and Docker.

Goes over:
- Microsoft
- .NET Framework (history)
- .NET Core
  - C# Web API
  - F# Suave Web API
- Docker
  - Microsoft maintianed docker images
  - how to build an image
  - how to run an image
- Briefly mention a 'real app' (trakr)

